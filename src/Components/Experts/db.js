 const data = [
    {
        "id": 1,
        "Pictures": "https://firebasestorage.googleapis.com/v0/b/mahorat-management.appspot.com/o/Khudoynazar.svg?alt=media&token=ee4ea741-1b3e-49e7-baa2-3e85285b1ec3",
        "Titile": "Khudaynazar Kurbanov",
         "Level": `Founder/CEO`,
        "Level2": "Mahorat & Management",
        "LinkedinLink": "https://www.linkedin.com/in/khudaynazar-kurbanov-70950723b",
        "Description": "Researcher (in the field of vocational education). Specialist with international experience in the fields of NQF, ISCED (UNESCO), Quality Assurance in Education. Content developer. Project manager. Co-founder of “Management & Management” organization. Has 18+ years of consulting experience in international and national projects."
    },
    {
        "id": 2,
        "Pictures": "https://firebasestorage.googleapis.com/v0/b/mahorat-management.appspot.com/o/Feruza.svg?alt=media&token=e1087542-93f6-44dd-bd42-17707da994c1",
        "Titile": "Feruza Rashidova ",
        "Level": `QA Expert in Education system`,
        "Level2": "PhD in Pedagogy",
        "LinkedinLink": "https://www.linkedin.com/in/dr-feruza-rashidova-01438326",
        "Description": "Doctor of Science in Pedagogy. An expert on the quality of education system for more than 20 years. Expert and trainer in ensuring the quality of educational programs and systems for more than 10 years. Content developer. Project manager. Leading expert of “Management & Management” organization as with the rich experience in consulting, developing the international projects on enhancement of HRD as systematic approach on Quality Assurance establishment in management, establishing the standards of qualifications of the teachers and personnel in education systems for sustainable development of local educational providers."
     },
    {
        "id": 3,
        "Pictures": "https://firebasestorage.googleapis.com/v0/b/mahorat-management.appspot.com/o/Shuhratxoja.svg?alt=media&token=e14de255-4e0c-4c31-8a51-3f810d15f2fa",
        "Titile": "Shuhratxo’ja Amanov   ",
        "Level": "Skills & Employment expert",
        "Level2": "Education",
        "LinkedinLink": "https://www.linkedin.com/in/shukhrathoja-amanov-mba-916a3513/",
        "Description": "Holds an MBA degree in . 20+ years working experience for the International Organizations in the areas of Business Development, Education, Skills and Human Capital Development. Several years' experience in management of and consultancy to donor and international lending agency funded projects in Armenia, Georgia, Kazakhstan, Tajikistan, Turkmenistan and Uzbekistan."
    },
    {
        "id": 4,
        "Pictures": "https://firebasestorage.googleapis.com/v0/b/mahorat-management.appspot.com/o/Farhod.svg?alt=media&token=984056c1-81c6-47c6-9228-6b7cb09e0199",
        "Titile": "Farhod Furkatov",
        "Level": "Data Scientist",
        "Level2": "CEO - Goodly Wellness",
        "LinkedinLink": "https://www.linkedin.com/in/farhodf",
        "Description": "National Expert of “Management & Management” organization. He has over 10 years of experience building and implementing digital products",
        "Description2": "His journey to the Tech world started with an introduction to the Data world in California, Silicon Valley. He is a tech entrepreneur with a passion to deliver quality tech solutions for businesses and people."
     },
    {
        "id": 5,
        "Pictures": "https://firebasestorage.googleapis.com/v0/b/mahorat-management.appspot.com/o/Sultanmurad.svg?alt=media&token=f60b99d3-2932-4cea-a0aa-8f4ab39bc7df",
        "Titile": "Sultanmurad Hamroev",
        "Level": "Business Analyst",
        "Level2": "Project Coordinator - TVET",
        "Description": "Expert on the quality of professional education. National expert in Mahorat & Management. Head of Young analytics development project (YADP)."
    },
     {
         "id": 6,
         "Pictures": "https://firebasestorage.googleapis.com/v0/b/mahorat-management.appspot.com/o/Alisher.svg?alt=media&token=3baf17d3-f601-4c9d-82c8-2949d55ddb5d",
         "Titile": " Alisher Akmaljonov ",
         "Level": "Entrepreneur/Founder of",
         "Level2": "“Event Entertainment” LLC",
         "LinkedinLink": "https://www.linkedin.com/in/alisher-akmaljonov-447125233",
         "Description": "Entrepreneur.Founder and director of the “Event Entertainment” LLC. Member of Central Asian Youth Leadership Academy (CAYLA). "
     },
     {
         "id": 7,
         "Pictures": "https://firebasestorage.googleapis.com/v0/b/mahorat-management.appspot.com/o/Sadokat.svg?alt=media&token=c5923621-d6fb-4812-abff-2baa22fc5704",
         "Titile": "Sadokat Kaziyeva",
         "Level": "Product Designer",
         "Level2": "User Experience specialist",
         "LinkedinLink": "https://www.linkedin.com/in/sadokat",
         "Description": "Sadokat made a transition into UI/UX Design field after many years in Human Resources and Recruiting. Enthusiastic about helping people solve their problems ensuring the best possible outcome. She has a work experience with one of the leading companies in the world - Apple.  She aspires  to help people transform their ideas into products that are simple, appealing, and harmonious."
     },
    {
        "id": 8,
        "Pictures": "https://firebasestorage.googleapis.com/v0/b/mahorat-management.appspot.com/o/Sirojiddin.svg?alt=media&token=630e5349-3437-4444-86c3-11fbc4da4b11",
        "Titile": "Sirojiddin Olimov",
        "Level": "Member of Central Asian",
        "Level2": "Youth Leadership Academy  \n (CAYLA)",
        "LinkedinLink": "https://www.linkedin.com/in/sirojiddinolimov",
        "Description": "Member of Central Asian Youth Leadership Academy (CAYLA). Master Program student of Edinburgh University"
    },
    {
        "id": 9,
        "Pictures": "https://firebasestorage.googleapis.com/v0/b/mahorat-management.appspot.com/o/Nodir.svg?alt=media&token=61ad25ab-6403-4f90-94a6-f5605af51257",
        "Titile": "Nodir Ergashev",
        "Level": "UzTEA expert",
        "Description": "UzTEA expert. Member of the Management Board of UzTEA.Content developer and trainer."
    },
    {
        "id": 10,
        "Pictures": "https://firebasestorage.googleapis.com/v0/b/mahorat-management.appspot.com/o/Sabrina.svg?alt=media&token=0e737666-8157-4764-ac4c-dc8503286afd",
        "Titile": "Sabrina Bakhtiyorova  ",
        "Level": "Project development",
        "Level2": "coordinator",
        "LinkedinLink": "https://www.linkedin.com/in/sabrina-bakhtiyorova-201694207",
        "Description": "Young analytics development project activist. Bachelor of Business and Financial Management (MDIST). Methodist."
    }
]

export {data}